'use strict';

var gulp = require('gulp');
var config = require('../config');

gulp.task('watch', ['browserSync'], function() {
	gulp.watch(config.paths.scss.all, ['styles']);
	gulp.watch(config.paths.views.all, ['views']);
	gulp.watch(config.paths.videos.all, ['videos']);
	gulp.watch(config.paths.json.all, ['json']);
	gulp.watch(config.paths.fonts.all, ['fonts']);
	gulp.watch(config.paths.js.all, ['lint', 'browserify']);
});
