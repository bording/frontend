'use strict';

var changed = require('gulp-changed');
var gulp = require('gulp');

var config = require('../config');

gulp.task('videos', function() {
	return gulp.src(config.paths.videos.all)
		.pipe(changed(config.paths.videos.dest))
		.pipe(gulp.dest(config.paths.videos.dest));
});
