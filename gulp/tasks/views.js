'use strict';


var gulp = require('gulp');

var fs = require('fs');
var config = require('../config');
var gulpif = require('gulp-if');
var browserSync = require('browser-sync');
var gih = require('gulp-include-html');

// Views task
gulp.task('views', function() {


	return gulp.src(config.paths.views.all)
		.pipe(gih({
			'public': config.paths.views.dest,
			baseDir: './src/html/',
		}))
		.pipe(gulp.dest(config.paths.views.dest))
		.pipe(gulpif(browserSync.active, browserSync.reload({stream: true})));
});
