'use strict';

var gulp = require('gulp');
var eslint = require('gulp-eslint');
var config = require('../config');


function isFixed(file) {
	// Has ESLint fixed the file contents?
	return file.eslint != null && file.eslint.fixed;
}

gulp.task('lint', function () {
	return gulp.src([config.paths.js.all])
		.pipe(eslint({
			fix: true
		}))
		.pipe(eslint.format())
		.pipe(eslint.failAfterError());
});
