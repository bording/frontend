'use strict';

var config = require('../config');
var handleErrors = require('../util/handleErrors');

var aliasify = require('aliasify');
var babelify = require('babelify');
var browserify = require('browserify');
var bulkify = require('bulkify');
var gulp = require('gulp');
var gulpif = require('gulp-if');
var gutil = require('gulp-util');
var source = require('vinyl-source-stream');
var uglifyify = require('uglifyify');
var browserifyInc = require('browserify-incremental');
var streamify = require('gulp-streamify');
var uglify = require('gulp-uglify');
var replace = require('gulp-replace');
// Based on: http://blog.avisi.nl/2014/04/25/how-to-keep-a-fast-build-with-browserify-and-reactjs/

var bundler = browserify({
	entries: config.paths.js.entries.src,
	debug: process.env.npm_lifecycle_event === 'prod' ? false : true,
	cache: {},
	packageCache: {},
	fullPaths: process.env.npm_lifecycle_event === 'prod' ? false : true
});

if (process.env.npm_lifecycle_event !== 'prod') {
	browserifyInc(bundler, {
		cacheFile: './build/browserify-cache.json'
	});
}

var babelOptions = {
	sourceMapsAbsolute: false,
	compact: true,
	presets: [
		['env', {
			'browsers': 'last 2 versions'
		}]
	]
};

var transforms = [
	{
		tr: babelify,
		opt: babelOptions
	},
	{
		tr: bulkify,
		opt: null
	},
	{
		tr: aliasify,
		opt: config.aliasConfig
	},
];

if (process.env.npm_lifecycle_event === 'prod') {
	transforms.push({
		tr: uglifyify,
		opt: {
			global: true
		}
	});
}

transforms.forEach(function(transform) {
	bundler.transform(transform.tr, transform.opt);
});

function bundle(file) {
	var stream = bundler.bundle();

	gutil.log('Rebundle...');

	return stream
		.on('error', handleErrors)
		.pipe(source(file))
		.pipe(gulpif(global.isProd, replace('./assets/', config.CDNALIAS)))
		.pipe(gulpif(global.isProd, streamify(uglify().on('error', gutil.log))))
		.pipe(gulp.dest(config.paths.js.dest));
}

gulp.task('browserify', function() {
	return bundle(config.paths.js.entries.name);
});
