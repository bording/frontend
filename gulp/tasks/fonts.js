'use strict';

var changed = require('gulp-changed');
var gulp = require('gulp');

var config = require('../config');

gulp.task('fonts', function() {
	return gulp.src(config.paths.fonts.all)
		.pipe(changed(config.paths.fonts.dest))
		.pipe(gulp.dest(config.paths.fonts.dest));
});
