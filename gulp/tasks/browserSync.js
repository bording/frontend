'use strict';

var browserSync = require('browser-sync');
var gulp = require('gulp');

var config = require('../config');

gulp.task('browserSync', function() {
	browserSync.init({
		server: {
			baseDir: config.paths.views.dest
		}
	});
});