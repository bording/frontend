'use strict';

var changed = require('gulp-changed');
var gulp = require('gulp');

var config = require('../config');

gulp.task('json', function() {
	return gulp.src(config.paths.json.all)
		.pipe(changed(config.paths.json.dest))
		.pipe(gulp.dest(config.paths.json.dest));
});
