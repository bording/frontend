'use strict';

var gulp = require('gulp');
var gulpif = require('gulp-if');
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var mqpacker = require('css-mqpacker');
var postcsswillchange = require('postcss-will-change');
var postcssEasings = require('postcss-easings');
var nano = require('gulp-cssnano');
var sourcemaps = require('gulp-sourcemaps');
var singleCharset = require('postcss-single-charset');
var discardEmpty = require('postcss-discard-empty');
var discardComments = require('postcss-discard-comments');

var handleErrors = require('../util/handleErrors');
var config = require('../config');

var processors = [
	postcsswillchange,
	postcssEasings,
	mqpacker({
		sort: true
	}),
	autoprefixer({
		browsers: ['last 4 version']
	}),
	discardComments,
	discardEmpty,
	singleCharset,
];

gulp.task('styles', function() {
	return gulp.src(config.paths.scss.all)
		.pipe(gulpif(!global.isProd, sourcemaps.init()))
		.pipe(sass({includePaths: ['node_modules/'] })
			.on('error', handleErrors)
		)
		.pipe(postcss(processors))
		.pipe(gulpif(!global.isProd, sourcemaps.write()))
		.pipe(gulpif(global.isProd, nano()))
		.pipe(gulp.dest(config.paths.scss.dest));
});
