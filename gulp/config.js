'use strict';

module.exports = {
	serverport: 3000,
	paths: {
		clean: [
			'build/css/*.css',
			'build/js/*.js'
		],
		aliasConfig: {
			aliases: {
				node_modules: 'node_modules'
			},
			verbose: false
		},
		scss: {
			all: 'src/scss/**/*',
			entry: 'src/scss/app.scss',
			dest: 'build/css'
		},
		js: {
			all: 'src/js/**/*.js',
			entries: {
				src: 'src/js/main.js',
				name: 'main.js'
			},
			dest: 'build/js'
		},
		views: {
			entry: 'src/html/index.html',
			all: 'src/html/**/*.html',
			dest: 'build'
		},
		images: {
			all: 'src/assets/images/**/*',
			dest: 'build/assets/images'
		},
		fonts: {
			all: 'src/assets/fonts/**/*',
			dest: 'build/assets/fonts'
		},
		videos: {
			all: 'src/assets/videos/**/*.mp4',
			dest: 'build/assets/videos'
		},
		json: {
			all: 'src/assets/json/**/*.json',
			dest: 'build/assets/json'
		}
	}
};
