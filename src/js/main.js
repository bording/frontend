'use stirct';

require('babel-polyfill');
require('bootstrap');

/*eslint-disable */
var jQuery = require('jquery');
window.jQuery = jQuery;
/*eslinst-enable */

var Lazy = require('jquery-lazy');


const feather = require('feather-icons');

var init = function () {

	feather.replace({
		'stroke-width': 1.8,
		'width': 20,
		'height': 20
	});

	// Dropdown functionality for sidebar
	jQuery('.dropdown-btn').click(function () {
		// checks if any drop down is active
		if (jQuery('.dropdown').hasClass('active')) {
			// checks if parent dropdown is active. If not active, remove all dropdown classes and hide open dropdowns
			if (!jQuery(this).parents('.dropdown').hasClass('active')) {
				jQuery('.dropdown-btn').removeClass('active');
				jQuery('.dropdown').removeClass('active');
				jQuery('.collapse').collapse('hide');
			}
		}
		jQuery(this).toggleClass('active');
		jQuery(this).parent().parent().toggleClass('active');
	});

	// Burger menu functionality
	// Open
	var jsOpenSidebar = jQuery('.js-open-sidebar');
	if (jsOpenSidebar.length)  {
		jQuery('.js-open-sidebar').click(function () {
			jQuery('.js-sidebar').addClass('active');
			jQuery('.js-main-content').addClass('active');
			jQuery('.js-body').css('overflow', 'hidden');
		});
	}

	// Close
	var jsCloseSidebar = jQuery('.js-close-sidebar');
	if (jsCloseSidebar.length)  {
		jQuery('.js-close-sidebar').click(function () {
			jQuery('.js-sidebar').removeClass('active');
			jQuery('.js-main-content').removeClass('active');
			jQuery('.js-body').css('overflow', 'auto');
		});
	}

	// Toggle grid/list view functionality
	// List
	var toggleList = jQuery('.js-toggle-list');
	if (toggleList.length) {
		jQuery('.js-toggle-list').click(function () {
			jQuery('.js-product-overview').addClass('list-view');
			jQuery('.js-product-overview').removeClass('grid-view');
		});
	}

	// Grid
	var toggleGrid = jQuery('.js-toggle-grid');
	if (toggleGrid.length)  {
		jQuery('.js-toggle-grid').click(function () {
			jQuery('.js-product-overview').removeClass('list-view');
			jQuery('.js-product-overview').addClass('grid-view');
		});
	}

	// Toggle Favorite item
	var favItem = jQuery('.js-favorite-item');
	if (favItem.length) {
		jQuery('.js-favorite-item').click(function () {
			jQuery(this).toggleClass('favorite');
		});
	}

	// Product config logic
	var quantitySelect = jQuery('.js-quantity-select');
	if (quantitySelect.length) {
		jQuery('.js-quantity-select').change(function () {
			const minVal = 7500;
			const maxVal = 25000;
			let curVal = jQuery(this).val();
			jQuery('.js-delivery-type-select').children().removeAttr('hidden');

			if (curVal > maxVal) {
				jQuery('.js-delivery-type-select').children().removeAttr('selected');
				jQuery('.js-delivery-type-select').children("[value='standard']").attr('selected', '');
				jQuery('.js-delivery-type-select').children("[value!='standard']").attr('hidden', '');
			} else if (curVal > minVal) {
				jQuery('.js-delivery-type-select').children("[value='overnight']").attr('hidden', '');
				jQuery('.js-delivery-type-select').children("[value='overnight']").removeAttr('selected');
			}
		});
	}

	// Sticky header functionality
	window.onscroll = function () {
		checkSticky();
	};

	var header = jQuery('.main-header');
	var mainContent = jQuery('#main-content');

	var sticky = header.offset();

	function checkSticky() {
		if (header.length && mainContent.length) {
			if (window.pageYOffset > sticky.top) {
				header.addClass('sticky-header');
				mainContent.addClass('sticky-header');
			} else {
				header.removeClass('sticky-header');
				mainContent.removeClass('sticky-header');
			}
		}
	}

	// simulated lazyload
	var lazyItems = jQuery('.lazy');
	if (lazyItems.length) {
		jQuery('.lazy').Lazy({
			effect: "fadeIn",
			effectTime: 100
		});
	}
};

//Show on hover list
var aShowCategory = document.getElementsByClassName('topCat');
for (var i = 0; i < aShowCategory.length; i++) {
	aShowCategory[i].addEventListener('mouseover', function(){
		var aCategory = document.getElementsByClassName('tab-cat');
		for (var j = 0; j < aCategory.length; j++) {
			aShowCategory[j].style.color = 'black';
			aCategory[j].style.display = 'none';
		}
		this.style.color = '#53C1FF';
		var sDataAttibute = this.getAttribute('data-showCategory');
		document.getElementById(sDataAttibute).style.display = 'block';

  	});
}

//Template-input print click
var btnPrintTempInput = document.getElementById('temp-input-print');
var overlayDiv = document.getElementById('box__print-clicked');


if(btnPrintTempInput && btnPrintTempInput.length > 0) {
	btnPrintTempInput.addEventListener('click', function(){
		overlayDiv.style.display = 'block';
	});
	var btnCloseTempOverlay = document.getElementById('customize-temp-overlay-close');
	btnCloseTempOverlay.addEventListener('click', function(){
		overlayDiv.style.display = 'none';
	});
}

document.addEventListener('DOMContentLoaded', init);
